from typing import Any
from typing import Dict


def sig_create(
    hub, module_name: str, module_output_directory_path: str, sls_data: Dict[str, Any]
):
    ...
