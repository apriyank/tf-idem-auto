import re
from collections import ChainMap
from typing import Any
from typing import Dict

from idem_codegen.idem_codegen.tool.utils import (
    separator,
)

__contracts__ = ["parameterize"]

attributes_to_ignore = ["resource_id", "arn"]


def convert_attribute_value_to_params(
    hub,
    resolved_value,
    attribute_key,
    attribute_value,
    tf_resource_key,
    additional_function=None,
):
    if not resolved_value or not isinstance(resolved_value, str):
        return
    parameterized_value = None

    # TODO: Uncomment this line after we add account_id as a mandatory param in params.sls
    # resolved_value = resolved_value.replace("${data.aws_caller_identity.current.account_id}", 'params.get("account_id")')

    while re.search(r"\${var\.[\w-]+}", resolved_value):
        resolved_value = re.sub(
            r"\${var\.[\w-]+}",
            hub.tf_idem.tool.generator.parameterizer.utils.convert_var_to_param,
            resolved_value,
        )
        parameterized_value = resolved_value

    if resolved_value and attribute_key == "tags":
        parameterized_value = (
            hub.tf_idem.exec.generator.parameterizer.parameterize.format_tf_tags(
                resolved_value, attribute_value, parameterized_value
            )
        )

    return parameterized_value


def parameterize(hub, sls_data: Dict[str, Any]):
    """
    This function takes sls_data as input loop through all the attribute values and
    check if they can be parameterized and convert ${var.test_variable} in terraform
    to {{params.get("test_variable")}} in sls.

    :param hub:
    :param sls_data:
    :return:
    """

    # get terraform resources_map. It's map of all terraform resources
    # we are trying to convert to sls

    terraform_resource_map = hub.tf_idem.RUNS["TF_RESOURCE_MAP"]
    complete_dict_of_variables = hub.tf_idem.RUNS["TF_VARIABLES"]
    # If we do not have terraform_resource_map we cannot parameterize,
    # raise an error if it's not found in hub

    if not terraform_resource_map:
        hub.log.warning(
            "Not able to parameterize, Terraform resource map is not found in hub."
        )
        return sls_data
    # ToDO: Simplify more
    for item in sls_data:
        resource_attributes = list(sls_data[item].values())[0]
        resource_type = list(sls_data[item].keys())[0].replace(".present", "")
        resource_map = dict(ChainMap(*resource_attributes))

        # get terraform resource to look for parameters used
        terraform_resource = terraform_resource_map.get(
            f"{resource_type}{separator}{resource_map.get('resource_id')}"
        )

        # if not terraform_resource:
        #     continue
        # loop through attributes
        for resource_attribute in resource_attributes:
            for (
                resource_attribute_key,
                resource_attribute_value,
            ) in resource_attribute.items():

                if (
                    terraform_resource
                    and resource_attribute_key not in attributes_to_ignore
                ):
                    (
                        tf_resource_value,
                        tf_resource_key,
                        is_attribute_different,
                    ) = hub.tf_idem.tool.utils.get_tf_equivalent_idem_attribute(
                        terraform_resource,
                        list(terraform_resource.keys())[0],
                        resource_attribute_key,
                    )

                    if isinstance(tf_resource_value, str) and re.search(
                        r"\${jsonencode\(var\.[\w-]+\)}", tf_resource_value
                    ):
                        tf_resource_value = re.sub(
                            r"\${jsonencode\(var\.[\w-]+\)}",
                            lambda jsonencode_string: f'"${{var.{jsonencode_string.group()[17:-2]}}}"',
                            tf_resource_value,
                        )

                    # call the recursive function on the attribute value
                    parameterized_value = hub.idem_codegen.tool.nested_iterator.recursively_iterate_over_resource_attribute(
                        tf_resource_value,
                        hub.tf_idem.generator.parameterizer.default.convert_attribute_value_to_params,
                        attribute_key=resource_attribute_key,
                        attribute_value=resource_attribute_value,
                        tf_resource_key=tf_resource_key,
                        additional_function=hub.tf_idem.exec.generator.parameterizer.parameterize.format_tags,
                    )
                    if parameterized_value:
                        if is_attribute_different:
                            hub.tf_idem.tool.utils.set_tf_equivalent_idem_attribute(
                                resource_attribute_key,
                                parameterized_value,
                                list(terraform_resource.keys())[0],
                                resource_attribute_key,
                                resource_attribute,
                            )
                        else:
                            resource_attribute[
                                resource_attribute_key
                            ] = parameterized_value

                    elif not isinstance(resource_attribute_value, bool):
                        attribute_in_variable = hub.tf_idem.exec.generator.parameterizer.parameterize.is_attr_value_in_variables(
                            resource_attribute_value, complete_dict_of_variables
                        )
                        if attribute_in_variable:
                            resource_attribute[
                                resource_attribute_key
                            ] = f'{{{{params.get("{attribute_in_variable}")}}}}'
                elif resource_attribute_key == "resource_id":
                    resource_attribute[
                        resource_attribute_key
                    ] = f'{{{{ params.get("{item}")}}}}'
    return sls_data
