## key for credstash encryption
resource "aws_kms_key" "credstash_key" {
  description = "Key for encrypting and decrypting in xyz-${var.clusterName} credstash"
  tags = merge(
    local.tags,
    {
      "Name" = "xyz-${var.clusterName}_credstash_key"
    },
  )

  depends_on = [
    aws_iam_role.cluster-node,
    aws_iam_role.xyz-admin,
  ]

  policy = <<EOF
{
  "Id": "key-consolepolicy-3",
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "Enable IAM User Permissions",
      "Effect": "Allow",
      "Principal": {
        "AWS": [
          "arn:aws:iam::${data.aws_caller_identity.current.account_id}:root"
        ]
      },
      "Action": "kms:*",
      "Resource": "*"
    },
    {
      "Sid": "Allow access for Key Administrators",
      "Effect": "Allow",
      "Principal": {
        "AWS": [
          "${aws_iam_role.xyz-admin.arn}",
          "${aws_iam_role.cluster-node.arn}"
        ]
      },
      "Action": [
        "kms:Create*",
        "kms:Describe*",
        "kms:Enable*",
        "kms:List*",
        "kms:Put*",
        "kms:Update*",
        "kms:Revoke*",
        "kms:Disable*",
        "kms:Get*",
        "kms:Delete*",
        "kms:TagResource",
        "kms:UntagResource",
        "kms:ScheduleKeyDeletion",
        "kms:CancelKeyDeletion"
      ],
      "Resource": "*"
    },
    {
        "Sid": "allow-cluster-node-decrypt-kms",
        "Effect": "Allow",
        "Principal": {
            "AWS": "${aws_iam_role.cluster-node.arn}"
        },
        "Action": "kms:Decrypt",
        "Resource": "*"
    }
  ]
}
EOF

}

output "aws_kms_key-credstash_key-key-id" {
  value = aws_kms_key.credstash_key.key_id
}

output "aws_kms_key-credstash_key-arn" {
  value = aws_kms_key.credstash_key.arn
}
