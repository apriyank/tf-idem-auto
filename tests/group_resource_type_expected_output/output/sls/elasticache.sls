elasticache-subnet-group-idem-test:
  aws.elasticache.cache_subnet_group.present:
  - name: elasticache-subnet-group-idem-test
  - resource_id: elasticache-subnet-group-idem-test
  - cache_subnet_group_description: For elastcache redis cluster
  - arn: arn:aws:elasticache:eu-west-3:123456789012:subnetgroup:elasticache-subnet-group-idem-test
  - subnet_ids:
    - subnet-039e53122e038d38c
    - subnet-05dfaa0d01a337199
    - subnet-050732fa4616470d9
  - tags: []
