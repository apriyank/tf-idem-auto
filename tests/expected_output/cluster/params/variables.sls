VpcName: ''
VpcSuperNet: 10.170.
admin_users:
- user1
- user2
- user3
- user4
- user5
automation: 'True'
aws_iam_role-cluster-node-arn: ''
aws_iam_role-xyz-admin: ''
aws_kms_key-credstash_key-arn: ''
aws_kms_key-credstash_key-key-id: ''
bastion_security_group_cidr:
  mango_us-west-2: 172.22.0.0/16
  scaleperf_us-west-2: 172.22.0.0/16
  sym-dev4_us-west-2: 172.16.0.0/16
  sym-prod_ap-northeast-1: 10.223.0.0/16
  sym-prod_ap-southeast-1: 10.223.0.0/16
  sym-prod_ap-southeast-2: 10.223.0.0/16
  sym-prod_ca-central-1: 10.223.0.0/16
  sym-prod_eu-central-1: 10.223.0.0/16
  sym-prod_eu-west-2: 10.223.0.0/16
  sym-prod_sa-east-1: 10.223.0.0/16
  sym-prod_us-west-2: 10.223.0.0/16
  sym-staging_ap-northeast-1: 172.25.0.0/16
  sym-staging_eu-west-2: 172.22.0.0/16
  sym-staging_us-west-2: 172.22.0.0/16
clusterName: idem-test
clusterVersion: '1.20'
cluster_admin:
- user1
cluster_edit:
- user1
cluster_public_subnet_cidr: '[""]'
cluster_pvt_subnet_cidr: '[""]'
cluster_read:
- user1
cogs: OPEX
create_subnets: ''
create_vpc: ''
cross_account_beachops_domain_profile: ''
enable-jenkins-rolling-upgrade-policy: 'True'
local_bastion_security_group_cidr_all: '${contains(keys(var.bastion_security_group_cidr),"${var.profile}_${var.region}")
  ? var.bastion_security_group_cidr : merge(var.bastion_security_group_cidr,{''${var.profile}_${var.region}'':
  ''${var.VpcSuperNet}0.0/16''})}'
local_tags:
  Automation: 'True'
  COGS: OPEX
  Environment: test-dev
  KubernetesCluster: idem-test
  Owner: org1
msk_kafka_domain: 'True'
owner: org1
profile: test-dev
public_subnet_name: ''
pvt_subnet_name: ''
region: eu-west-3
singleAz: 'True'
transit-gw: 0
