# ToDo: The attribute 'zone_id' has resolved value of 'data' state. Please create a variable with resolved value and use { params.get('variable_name') } instead of resolved value of 'data' state.
# ToDo: The attribute 'vpc_id' has resolved value of 'data' state. Please create a variable with resolved value and use { params.get('variable_name') } instead of resolved value of 'data' state.
aws_route53_zone_association.internal-potato-beachops-io:
  aws.route53.hosted_zone_association.present:
  - resource_id: {{ params.get("aws_route53_zone_association.internal-potato-beachops-io")}}
  - name: Z0721725PI001CQXUGYY:vpc-0738f2a523f4735bd:eu-west-3
  - zone_id: Z0721725PI001CQXUGYY
  - vpc_id: ${aws.ec2.vpc:aws_vpc.cluster:resource_id}
  - vpc_region: {{params.get("region")}}
  - comment:


# ToDo: The attribute 'zone_id' has resolved value of 'data' state. Please create a variable with resolved value and use { params.get('variable_name') } instead of resolved value of 'data' state.
# ToDo: The attribute 'vpc_id' has resolved value of 'data' state. Please create a variable with resolved value and use { params.get('variable_name') } instead of resolved value of 'data' state.
aws_route53_zone_association.msk_kafka_domain:
  aws.route53.hosted_zone_association.present:
  - resource_id: {{ params.get("aws_route53_zone_association.msk_kafka_domain")}}
  - name: Z03616682JNY9P3D3T2IR:vpc-0738f2a523f4735bd:eu-west-3
  - zone_id: Z03616682JNY9P3D3T2IR
  - vpc_id: ${aws.ec2.vpc:aws_vpc.cluster:resource_id}
  - vpc_region: {{params.get("region")}}
  - comment:
