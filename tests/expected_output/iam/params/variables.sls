admin_users:
- user1
- user2
- user3
- user4
- user5
automation: 'True'
clusterName: idem-test
cogs: OPEX
local_tags:
  Automation: 'True'
  COGS: OPEX
  Environment: test-dev
  KubernetesCluster: idem-test
  Owner: org1
owner: org1
profile: test-dev
region: eu-west-3
singleAz: 'True'
