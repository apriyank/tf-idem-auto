dopt-00338f328b40be8f0:
  aws.ec2.dhcp_option.present:
  - name: dopt-00338f328b40be8f0
  - resource_id: dopt-00338f328b40be8f0
  - tags:
    - Key: test_name222
      Value: test-dhcp333
  - dhcp_configurations:
    - Key: domain-name
      Values:
      - testing123
    - Key: domain-name-servers
      Values:
      - 10.2.5.1
      - 10.2.5.2
    - Key: netbios-node-type
      Values:
      - '2'


dopt-00756d50a1f5125a0:
  aws.ec2.dhcp_option.present:
  - name: dopt-00756d50a1f5125a0
  - resource_id: dopt-00756d50a1f5125a0
  - tags:
    - Key: test_name222
      Value: test-dhcp333
  - dhcp_configurations:
    - Key: domain-name
      Values:
      - testing123
    - Key: domain-name-servers
      Values:
      - 10.2.5.1
      - 10.2.5.2
    - Key: netbios-node-type
      Values:
      - '2'


dopt-0131e72ab5197b3ef:
  aws.ec2.dhcp_option.present:
  - name: dopt-0131e72ab5197b3ef
  - resource_id: dopt-0131e72ab5197b3ef
  - tags:
    - Key: test_name222
      Value: test-dhcp333
  - dhcp_configurations:
    - Key: domain-name
      Values:
      - testing123
    - Key: domain-name-servers
      Values:
      - 10.2.5.1
      - 10.2.5.2
    - Key: netbios-node-type
      Values:
      - '2'


dopt-014ff111b2c222df6:
  aws.ec2.dhcp_option.present:
  - name: dopt-014ff111b2c222df6
  - resource_id: dopt-014ff111b2c222df6
  - tags:
    - Key: test_name222
      Value: test-dhcp333
  - dhcp_configurations:
    - Key: domain-name
      Values:
      - testing123
    - Key: domain-name-servers
      Values:
      - 10.2.5.1
      - 10.2.5.2
    - Key: netbios-node-type
      Values:
      - '2'


dopt-0155e2b56140c3584:
  aws.ec2.dhcp_option.present:
  - name: dopt-0155e2b56140c3584
  - resource_id: dopt-0155e2b56140c3584
  - tags:
    - Key: KubernetesCluster
      Value: idem-test
    - Key: Name
      Value: xyz-idem-test
    - Key: Automation
      Value: 'true'
    - Key: Owner
      Value: org1
    - Key: COGS
      Value: OPEX
    - Key: Environment
      Value: test-dev
  - dhcp_configurations:
    - Key: domain-name
      Values:
      - us-west-2.compute.internal
    - Key: domain-name-servers
      Values:
      - AmazonProvidedDNS


dopt-036afdf595a5d3bb5:
  aws.ec2.dhcp_option.present:
  - name: dopt-036afdf595a5d3bb5
  - resource_id: dopt-036afdf595a5d3bb5
  - tags: []
  - dhcp_configurations:
    - Key: domain-name-servers
      Values:
      - 10.2.5.1
      - 10.2.5.2


dopt-0480c4df211668e3a:
  aws.ec2.dhcp_option.present:
  - name: dopt-0480c4df211668e3a
  - resource_id: dopt-0480c4df211668e3a
  - tags:
    - Key: Environment
      Value: test-dev
    - Key: KubernetesCluster
      Value: idem-test
    - Key: Automation
      Value: 'true'
    - Key: COGS
      Value: OPEX
    - Key: Owner
      Value: org1
    - Key: Name
      Value: xyz-idem-test
  - dhcp_configurations:
    - Key: domain-name
      Values:
      - us-west-2.compute.internal
    - Key: domain-name-servers
      Values:
      - AmazonProvidedDNS


dopt-04be1a8d8d0826a24:
  aws.ec2.dhcp_option.present:
  - name: dopt-04be1a8d8d0826a24
  - resource_id: dopt-04be1a8d8d0826a24
  - tags:
    - Key: test_name222
      Value: test-dhcp333
  - dhcp_configurations:
    - Key: domain-name
      Values:
      - testing123
    - Key: domain-name-servers
      Values:
      - 10.2.5.1
      - 10.2.5.2
    - Key: netbios-node-type
      Values:
      - '2'


dopt-052512c363a0a800b:
  aws.ec2.dhcp_option.present:
  - name: dopt-052512c363a0a800b
  - resource_id: dopt-052512c363a0a800b
  - tags:
    - Key: COGS
      Value: OPEX
    - Key: Owner
      Value: org1
    - Key: Name
      Value: xyz-idem-test
    - Key: Environment
      Value: test-dev
    - Key: Automation
      Value: 'true'
    - Key: KubernetesCluster
      Value: idem-test
  - dhcp_configurations:
    - Key: domain-name
      Values:
      - us-west-2.compute.internal
    - Key: domain-name-servers
      Values:
      - AmazonProvidedDNS


dopt-0656233c5899a635a:
  aws.ec2.dhcp_option.present:
  - name: dopt-0656233c5899a635a
  - resource_id: dopt-0656233c5899a635a
  - tags: []
  - dhcp_configurations:
    - Key: domain-name-servers
      Values:
      - 10.2.5.1
      - 10.2.5.2


dopt-06763700a2b49ea57:
  aws.ec2.dhcp_option.present:
  - name: dopt-06763700a2b49ea57
  - resource_id: dopt-06763700a2b49ea57
  - tags:
    - Key: test_name222
      Value: test-dhcp333
  - dhcp_configurations:
    - Key: domain-name
      Values:
      - testing123
    - Key: domain-name-servers
      Values:
      - 10.2.5.1
      - 10.2.5.2
    - Key: netbios-node-type
      Values:
      - '2'


dopt-078d9c1e9e0cd4bdb:
  aws.ec2.dhcp_option.present:
  - name: dopt-078d9c1e9e0cd4bdb
  - resource_id: dopt-078d9c1e9e0cd4bdb
  - tags:
    - Key: test_name222
      Value: test-dhcp333
  - dhcp_configurations:
    - Key: domain-name
      Values:
      - testing123
    - Key: domain-name-servers
      Values:
      - 10.2.5.1
      - 10.2.5.2
    - Key: netbios-node-type
      Values:
      - '2'


dopt-0882a9c4d825793ee:
  aws.ec2.dhcp_option.present:
  - name: dopt-0882a9c4d825793ee
  - resource_id: dopt-0882a9c4d825793ee
  - tags:
    - Key: COGS
      Value: OPEX
    - Key: Name
      Value: xyz-idem-test
    - Key: Environment
      Value: test-dev
    - Key: Automation
      Value: 'true'
    - Key: KubernetesCluster
      Value: idem-test
    - Key: Owner
      Value: org1
  - dhcp_configurations:
    - Key: domain-name
      Values:
      - us-west-2.compute.internal
    - Key: domain-name-servers
      Values:
      - AmazonProvidedDNS


dopt-0ae8c394c563d5ee8:
  aws.ec2.dhcp_option.present:
  - name: dopt-0ae8c394c563d5ee8
  - resource_id: dopt-0ae8c394c563d5ee8
  - tags:
    - Key: test_name222
      Value: test-dhcp333
  - dhcp_configurations:
    - Key: domain-name
      Values:
      - testing123
    - Key: domain-name-servers
      Values:
      - 10.2.5.1
      - 10.2.5.2
    - Key: netbios-node-type
      Values:
      - '2'


dopt-0b8e3e4566083cc5e:
  aws.ec2.dhcp_option.present:
  - name: dopt-0b8e3e4566083cc5e
  - resource_id: dopt-0b8e3e4566083cc5e
  - tags:
    - Key: test_name222
      Value: test-dhcp333
  - dhcp_configurations:
    - Key: domain-name
      Values:
      - testing123
    - Key: domain-name-servers
      Values:
      - 10.2.5.1
      - 10.2.5.2
    - Key: netbios-node-type
      Values:
      - '2'


dopt-0c038d5d17f706f51:
  aws.ec2.dhcp_option.present:
  - name: dopt-0c038d5d17f706f51
  - resource_id: dopt-0c038d5d17f706f51
  - tags:
    - Key: test_name222
      Value: test-dhcp333
  - dhcp_configurations:
    - Key: domain-name
      Values:
      - testing123
    - Key: domain-name-servers
      Values:
      - 10.2.5.1
      - 10.2.5.2
    - Key: netbios-node-type
      Values:
      - '2'


dopt-0f26285d884de8f6b:
  aws.ec2.dhcp_option.present:
  - name: dopt-0f26285d884de8f6b
  - resource_id: dopt-0f26285d884de8f6b
  - tags:
    - Key: Owner
      Value: org1
    - Key: COGS
      Value: OPEX
    - Key: KubernetesCluster
      Value: idem-test
    - Key: Environment
      Value: test-dev
    - Key: Automation
      Value: 'true'
    - Key: Name
      Value: xyz-idem-test
  - dhcp_configurations:
    - Key: domain-name
      Values:
      - us-west-2.compute.internal
    - Key: domain-name-servers
      Values:
      - AmazonProvidedDNS


dopt-68ef1501:
  aws.ec2.dhcp_option.present:
  - name: dopt-68ef1501
  - resource_id: dopt-68ef1501
  - tags: []
  - dhcp_configurations:
    - Key: domain-name
      Values:
      - eu-west-3.compute.internal
    - Key: domain-name-servers
      Values:
      - AmazonProvidedDNS
