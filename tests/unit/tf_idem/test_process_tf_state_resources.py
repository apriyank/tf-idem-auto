import json


def test_process_tf_state_resources(hub):
    list_of_modules = {"module.iam", "module.cluster"}
    tf_state_resources_path = (
        f"{hub.test.idem_codegen.current_path}/files/tf_state_resources.json"
    )
    with open(tf_state_resources_path) as file:
        data = file.read()
        tf_state_resources = json.loads(data)

    processed_tf_state_resources = (
        hub.tf_idem.exec.compiler.compile.process_tf_state_resources(
            tf_state_resources, list_of_modules
        )
    )

    assert processed_tf_state_resources
    assert tf_state_resources[0] in processed_tf_state_resources
    assert tf_state_resources[1] in processed_tf_state_resources
    assert tf_state_resources[2] in processed_tf_state_resources
    assert tf_state_resources[3] not in processed_tf_state_resources
